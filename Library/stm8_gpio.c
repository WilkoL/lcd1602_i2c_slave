/*
*	PB4 PB5 are ALWAYS open-drain
*	and do NOT have pullup resistors
*   (I2C ports SDA and SCL)
*/

#include "stm8_gpio.h"

void GPIO_DeConfig(void)
{
	GPIOA->ODR = (uint8_t) ~GPIO_PIN_ALL;			//reset all GPIO to defaults
	GPIOA->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOA->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOA->CR2 = (uint8_t) ~GPIO_PIN_ALL;
	
	GPIOB->ODR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOB->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOB->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOB->CR2 = (uint8_t) ~GPIO_PIN_ALL;
	
	GPIOC->ODR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOC->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOC->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOC->CR2 = (uint8_t) ~GPIO_PIN_ALL;
	
	GPIOD->ODR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOD->DDR = (uint8_t) ~GPIO_PIN_ALL;
	GPIOD->CR1 = (uint8_t) ~GPIO_PIN_ALL;
	GPIOD->CR2 = (uint8_t) ~GPIO_PIN_ALL;
}

void GPIO_Config(void)
{
	//led
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_4;
	GPIOD->DDR |= (uint8_t) GPIO_PIN_4;
	GPIOD->CR1 |= (uint8_t) GPIO_PIN_4;
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_4;
	
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_5;
	GPIOD->DDR |= (uint8_t) GPIO_PIN_5;
	GPIOD->CR1 |= (uint8_t) GPIO_PIN_5;
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_5;
	
	
	
	//LCD data
	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_4;
	GPIOC->DDR |= (uint8_t) GPIO_PIN_4;
	GPIOC->CR1 |= (uint8_t) GPIO_PIN_4;
	GPIOC->CR2 &= (uint8_t) ~GPIO_PIN_4;

	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_5;
	GPIOC->DDR |= (uint8_t) GPIO_PIN_5;
	GPIOC->CR1 |= (uint8_t) GPIO_PIN_5;
	GPIOC->CR2 &= (uint8_t) ~GPIO_PIN_5;
	
	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_6;
	GPIOC->DDR |= (uint8_t) GPIO_PIN_6;
	GPIOC->CR1 |= (uint8_t) GPIO_PIN_6;
	GPIOC->CR2 &= (uint8_t) ~GPIO_PIN_6;
	
	GPIOC->ODR &= (uint8_t) ~GPIO_PIN_7;
	GPIOC->DDR |= (uint8_t) GPIO_PIN_7;
	GPIOC->CR1 |= (uint8_t) GPIO_PIN_7;
	GPIOC->CR2 &= (uint8_t) ~GPIO_PIN_7;
	
	
	//LCD control
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_1;
	GPIOD->DDR |= (uint8_t) GPIO_PIN_1;
	GPIOD->CR1 |= (uint8_t) GPIO_PIN_1;
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_1;
	
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_2;
	GPIOD->DDR |= (uint8_t) GPIO_PIN_2;
	GPIOD->CR1 |= (uint8_t) GPIO_PIN_2;
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_2;
	
	GPIOD->ODR &= (uint8_t) ~GPIO_PIN_3;
	GPIOD->DDR |= (uint8_t) GPIO_PIN_3;
	GPIOD->CR1 |= (uint8_t) GPIO_PIN_3;
	GPIOD->CR2 &= (uint8_t) ~GPIO_PIN_3;
	
	
}
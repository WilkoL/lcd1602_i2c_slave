#include "stm8_exti.h"

volatile uint8_t exti_on_portc;

void EXTI_Config(void)
{
	disableInterrupts();
	
	EXTI->CR1 |= (2 << 4);							//enable EXTI on falling edge of PORTC
	EXTI->CR2 = 0;

	enableInterrupts();
}
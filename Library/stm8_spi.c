#include "stm8_spi.h"

void SPI_Config(void)
{
	disableInterrupts();
	
	//PA3 is used as SPI_CE
	GPIOA->ODR &= (uint8_t) ~GPIO_PIN_3;			//PA3 = output low
	GPIOA->DDR |= (uint8_t) GPIO_PIN_3;				//PA3 = output
	GPIOA->CR1 |= (uint8_t) GPIO_PIN_3;				//PA3 = output push pull
	GPIOA->CR2 |= (uint8_t) GPIO_PIN_3;				//PA3 = output high speed
	
	CLK->PCKENR1 |= CLK_PCKENR1_SPI;				//enable SPI Clock
	
	SPI_Init(SPI_FIRSTBIT_MSB, \
			SPI_BAUDRATEPRESCALER_16, \
			SPI_MODE_MASTER, \
			SPI_CLOCKPOLARITY_LOW, \
			SPI_CLOCKPHASE_1EDGE, \
			SPI_DIRECTION_TX, \
			SPI_NSS_SOFT, \
			0x00);
	
	SPI_Cmd(ENABLE);
	
	enableInterrupts();
}

void SPI_Send_16bits(uint16_t data)
{
	GPIOA->ODR &= (uint8_t) ~GPIO_PIN_3;
			
	while ((SPI->SR & SPI_SR_TXE) == 0);
	SPI_SendData((uint8_t) (data >> 8));	
	while ((SPI->SR & SPI_SR_TXE) == 0);
	SPI_SendData((uint8_t) (data & 0x00FF));		
	while (SPI->SR & SPI_SR_BSY);
	
	GPIOA->ODR |= GPIO_PIN_3;
}

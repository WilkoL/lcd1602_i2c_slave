#ifndef STM8_GPIO_H
#define STM8_GPIO_H

#include "stm8s.h"

void GPIO_DeConfig(void);
void GPIO_Config(void);

#endif

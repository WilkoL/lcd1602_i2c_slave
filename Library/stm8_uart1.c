#include "stm8_uart1.h"

#define UART1_RXBUFF_SIZE 64            //max 256
#define UART1_TXBUFF_SIZE 64            //max 256

#define UART1_RXBUFF_MASK	(UART1_RXBUFF_SIZE - 1)
#define UART1_TXBUFF_MASK	(UART1_TXBUFF_SIZE - 1)

#if (UART1_RXBUFF_SIZE & UART1_RXBUFF_MASK)
#error RX BUFFER SIZE is not a power of 2
#endif
#if (UART1_TXBUFF_SIZE & UART1_TX_BUFF_MASK)
#error UART1 TXBUFF SIZE is not a power of 2
#endif

volatile uint8_t UART1_rxbuff[UART1_RXBUFF_SIZE];
volatile uint8_t UART1_txbuff[UART1_RXBUFF_SIZE];
volatile uint8_t UART1_rxhead = 0;
volatile uint8_t UART1_rxtail = 0;
volatile uint8_t UART1_txhead = 0;
volatile uint8_t UART1_txtail = 0;
volatile uint16_t UART1_rxerror = 0;

const uint8_t UART1_rxbuff_size = UART1_RXBUFF_SIZE;
const uint8_t UART1_txbuff_size = UART1_TXBUFF_SIZE;
const uint8_t UART1_rxbuff_mask = UART1_RXBUFF_MASK;
const uint8_t UART1_txbuff_mask = UART1_TXBUFF_MASK;


void UART1_DeConfig(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 &= (uint8_t) ~CLK_PCKENR1_UART1;	//disable UART1 clocks
	
	(void)UART1->SR;								//clear status register
	(void)UART1->DR;								//clear (incoming)data register
  
	UART1->BRR2 = 0x00;								//reset all registers
	UART1->BRR1 = 0x00;
	UART1->CR1 = 0x00;
	UART1->CR2 = 0x00;
	UART1->CR3 = 0x00;
	UART1->CR4 = 0x00;
	UART1->GTR = 0x00;
	UART1->PSCR = 0x00;
	
	enableInterrupts();
}

void UART1_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 |= (uint8_t) CLK_PCKENR1_UART1;	//enable UART1 clocks
	
	//UART1->BRR2 = 0x03;								//baudrate = 9600
	//UART1->BRR1 = 0x68;								//at 16 MHz masterclock
	UART1->BRR2 = 0x0B;								//baudrate = 115200
	UART1->BRR1 = 0x08;								//at 16 MHz masterclock
	
	UART1->CR1 = 0x00;								//uart enabled, 8bits no parity
	UART1->CR2 |= UART1_CR2_TEN | UART1_CR2_REN;	//tx, rx enabled, no irqs
	UART1->CR2 |= UART1_CR2_RIEN;					//enable RXNE-irq
	UART1->CR3 = 0x00;								//1 stopbit, no SCLK
	UART1->CR4 = 0x00;								//no LIN
	UART1->GTR = 0x00;								//guard time?
	UART1->PSCR = 0x00;								//no IrDA
	
	enableInterrupts();
}


uint16_t UART1_getc(void)
{
	uint8_t tmp;
	uint16_t data;

	if (UART1_rxhead == UART1_rxtail)
	{
		return(0); //no data available
	}

	tmp = (uint8_t) ((UART1_rxtail + 1) & UART1_rxbuff_mask);
	UART1_rxtail = tmp;
	
	data = (uint16_t) ((UART1_rxerror << 8) | UART1_rxbuff[tmp]);
	
	return(data);
}


void UART1_putc(uint8_t data)
{
	uint8_t tmp;
	
	tmp = (uint8_t) ((UART1_txhead + 1) & UART1_txbuff_mask);
	while (tmp == UART1_txtail) {}                  //wait for free space in tx buffer
	UART1_txbuff[tmp] = data;
	UART1_txhead = tmp;
	UART1->CR2 |= (uint8_t) UART1_CR2_TIEN;			//enable TXE-irq
}


void UART1_puts(char *s)
{
	while (*s) UART1_putc(*s++);
}


uint16_t UART1_available(void)
{
	
	return (uint16_t) ((UART1_rxerror << 8) | ((UART1_rxbuff_mask + UART1_rxhead - UART1_rxtail) % UART1_rxbuff_mask));
}


void UART1_flush(void)
{
	UART1_rxhead = UART1_rxtail;
}



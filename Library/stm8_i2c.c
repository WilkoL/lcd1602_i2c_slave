#include "stm8_i2c.h"

void I2C_Config_Master(void)
{
	disableInterrupts();

	CLK->PCKENR1 |= CLK_PCKENR1_I2C;									//enable I2C Clock
	I2C_Init(I2C_MAX_STANDARD_FREQ, 0x00,I2C_DUTYCYCLE_2, I2C_ACK_CURR, I2C_ADDMODE_7BIT, 16);

	enableInterrupts();
}


void I2C_sendbyte_reg8bits(uint8_t reg8bits_address, uint8_t reg8bits_register, uint8_t databyte)
{
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
	
	I2C_Send7bitAddress(reg8bits_address, I2C_DIRECTION_TX);			//write-address
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) reg8bits_register);							//reg8bits register
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));	 		//byte is sent?
	
	I2C_SendData(databyte);												//send data byte
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));			//byte is sent?
	
	I2C_GenerateSTOP(ENABLE);											//stopbit
}


uint8_t I2C_readbyte_reg8bits(uint8_t reg8bits_address, uint8_t reg8bits_register)
{
	uint8_t tmp = 0;
	
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master mode?
		
	I2C_Send7bitAddress(reg8bits_address, I2C_DIRECTION_TX);			//write-address
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) reg8bits_register);							//register
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));	 		//byte is sent?
		
	I2C_GenerateSTART(ENABLE);											//repeat startbit
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
		
	I2C_Send7bitAddress(reg8bits_address, I2C_DIRECTION_RX);			//read-address
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));	//receiver-mode?
	
		
	I2C_AcknowledgeConfig(I2C_ACK_NONE);
	I2C_GenerateSTOP(ENABLE);											//stopbit
	while(I2C_GetFlagStatus( I2C_FLAG_RXNOTEMPTY) == RESET);
	tmp = I2C_ReceiveData();											//read byte
	//I2C_AcknowledgeConfig( I2C_ACK_CURR);
	
	return tmp;
}


void I2C_sendarray_reg8bits(uint8_t reg8bits_address, uint8_t reg8bits_register,  uint8_t *dataarray, uint8_t datalength)
{
	uint8_t i;
	
	i = 0;
	
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
	
	I2C_Send7bitAddress(reg8bits_address, I2C_DIRECTION_TX);				//write-address-of reg16bits
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) reg8bits_register);							//reg8bits register
	while(I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET);		//byte is sent?
	
	while (i < datalength)
	{
		I2C_SendData(dataarray[i++]);									//send reg16bits data byte
		while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));		//byte is sent?
	}
	I2C_GenerateSTOP(ENABLE);											//stopbit
}




void I2C_readarray_reg8bits(uint8_t reg8bits_address, uint16_t reg8bits_register, uint8_t *dataarray, uint8_t datalength)
{
	uint8_t i;
	
	i = 0;
	
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master mode?
		
	I2C_Send7bitAddress(reg8bits_address, I2C_DIRECTION_TX);				//write-address-of reg16bits
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?

	I2C_SendData((uint8_t) reg8bits_register);							//reg8bits register
	while(I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET);		//byte is sent?
		
	I2C_GenerateSTART(ENABLE);											//repeat startbit
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
		
	I2C_Send7bitAddress(reg8bits_address, I2C_DIRECTION_RX);				//read-address
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));	//receiver-mode?
	
	I2C_AcknowledgeConfig(I2C_ACK_CURR);	
	while (i < (datalength - 1))
	{
		while(I2C_GetFlagStatus( I2C_FLAG_RXNOTEMPTY) == RESET);
		dataarray[i++] = I2C_ReceiveData();								//read byte
	}
	
	I2C_AcknowledgeConfig(I2C_ACK_NONE);
	I2C_GenerateSTOP(ENABLE);											//stopbit
	while(I2C_GetFlagStatus( I2C_FLAG_RXNOTEMPTY) == RESET);
	dataarray[i] = I2C_ReceiveData();									//read last byte
}


//*****************************************************************************************



void I2C_sendbyte_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register, uint8_t databyte)
{
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
	
	I2C_Send7bitAddress(reg16bits_address, I2C_DIRECTION_TX);				//write-address-of reg16bits
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) (reg16bits_register & 0xFF00) >> 8);			//reg16bits register high
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));	 		//byte is sent?
	I2C_SendData((uint8_t) (reg16bits_register & 0x00FF));					//reg16bits register low
	while(I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET);		//byte is sent?
	
	I2C_SendData(databyte);												//send data byte
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));			//byte is sent?
	
	I2C_GenerateSTOP(ENABLE);											//stopbit
}


uint8_t I2C_readbyte_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register)
{
	uint8_t tmp = 0;
	
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master mode?
		
	I2C_Send7bitAddress(reg16bits_address, I2C_DIRECTION_TX);				//write-address-of reg16bits
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) (reg16bits_register & 0xFF00) >> 8);			//reg16bits register high
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));	 		//byte is sent?
	I2C_SendData((uint8_t) (reg16bits_register & 0x00FF));					//reg16bits register low
	while(I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET);		//byte is sent?
		
	I2C_GenerateSTART(ENABLE);											//repeat startbit
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
		
	I2C_Send7bitAddress(reg16bits_address, I2C_DIRECTION_RX);				//read-address
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));	//receiver-mode?
	
		
	I2C_AcknowledgeConfig(I2C_ACK_NONE);
	I2C_GenerateSTOP(ENABLE);											//stopbit
	while(I2C_GetFlagStatus( I2C_FLAG_RXNOTEMPTY) == RESET);
	tmp = I2C_ReceiveData();											//read byte
	//I2C_AcknowledgeConfig( I2C_ACK_CURR);
	
	return tmp;
}



void I2C_sendarray_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register,  uint8_t *dataarray, uint8_t datalength)
{
	uint8_t i;
	
	i = 0;
	
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
	
	I2C_Send7bitAddress(reg16bits_address, I2C_DIRECTION_TX);				//write-address-of reg16bits
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) (reg16bits_register & 0xFF00) >> 8);			//reg16bits register high
	//I2C_SendData((uint8_t) (reg16bits_register & 0x00FF));					//reg16bits register low
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));	 		//byte is sent?
	//I2C_SendData((uint8_t) (reg16bits_register & 0xFF00) >> 8);				//reg16bits register high
	I2C_SendData((uint8_t) (reg16bits_register & 0x00FF));					//reg16bits register low
	while(I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET);		//byte is sent?
	
	while (i < datalength)
	{
		I2C_SendData(dataarray[i++]);									//send reg16bits data byte
		while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));		//byte is sent?
	}
	I2C_GenerateSTOP(ENABLE);											//stopbit
}


void I2C_readarray_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register, uint8_t *dataarray, uint8_t datalength)
{
	uint8_t i;
	
	i = 0;
	
	I2C_GenerateSTART(ENABLE);											//startbit
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));				//master mode?
		
	I2C_Send7bitAddress(reg16bits_address, I2C_DIRECTION_TX);				//write-address-of reg16bits
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));//transmit-mode?
	
	I2C_SendData((uint8_t) (reg16bits_register & 0xFF00) >> 8);			//reg16bits register high
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED));	 		//byte is sent?
	I2C_SendData((uint8_t) (reg16bits_register & 0x00FF));					//reg16bits register low
	while(I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET);		//byte is sent?
		
	I2C_GenerateSTART(ENABLE);											//repeat startbit
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_MODE_SELECT));				//master-mode?
		
	I2C_Send7bitAddress(reg16bits_address, I2C_DIRECTION_RX);				//read-address
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));	//receiver-mode?
	
	I2C_AcknowledgeConfig(I2C_ACK_CURR);	
	while (i < (datalength - 1))
	{
		while(I2C_GetFlagStatus( I2C_FLAG_RXNOTEMPTY) == RESET);
		dataarray[i++] = I2C_ReceiveData();								//read byte
	}
	
	I2C_AcknowledgeConfig(I2C_ACK_NONE);
	I2C_GenerateSTOP(ENABLE);											//stopbit
	while(I2C_GetFlagStatus( I2C_FLAG_RXNOTEMPTY) == RESET);
	dataarray[i] = I2C_ReceiveData();									//read last byte
}

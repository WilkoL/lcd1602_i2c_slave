#include "stm8_adc1.h"

#define NR_ADC_CHANNELS	4							//nr of channels to be converted

volatile uint16_t ADC1_result[NR_ADC_CHANNELS];
volatile uint8_t ADC1_conv_ready;
const uint8_t ADC1_nr_of_channels = NR_ADC_CHANNELS;

void ADC1_DeConfig(void)
{
	disableInterrupts();
	
	ADC1->CSR  = 0x00;
	ADC1->CR1  = 0x00;
	ADC1->CR2  = 0x00;
	ADC1->CR3  = 0x00;
	ADC1->TDRH = 0x00;
	ADC1->TDRL = 0x00;
	ADC1->HTRH = 0xFF;
	ADC1->HTRL = 0x03;
	ADC1->LTRH = 0x00;
	ADC1->LTRL = 0x00;
	ADC1->AWCRH = 0x00;
	ADC1->AWCRL = 0x00;
	
	enableInterrupts();
}

void ADC1_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR2 = CLK_PCKENR2_ADC;					//enable ADC clock
	
	ADC1->CSR |= ADC1_nr_of_channels - 1;			//number of channels to be converted
	ADC1->CSR |= ADC1_CSR_EOCIE;					//enable EndOfConversion interrupt
	
	ADC1->CR1 |= (0x01 << 4);						//1 means fADC=FMASTER/3 (16/3 = 5.33MHz)
	
	ADC1->CR2 |= ADC1_CR2_ALIGN | ADC1_CR2_SCAN;	//right alignment, scan mode
	ADC1->CR2 |= ADC1_CR2_EXTTRIG;					//start on external-trigger event
	ADC1->CR2 |= (0x00 << 4);						//select TIM1_TRGO event
	
	ADC1->CR3 |= ADC1_CR3_DBUF;						//results in buffer (not necessary in scan mode?)
	
	ADC1->TDRH = 0x00;								//no schmitt triggers disabled
	ADC1->TDRL = 0x0F;								//schmitt triggers disabled for IN0, IN1, IN2, IN3)
	
	ADC1->HTRH = 0xFF;								//analog watchdog threshold high
	ADC1->HTRL = 0x03;								//analog watchdog threshold high
	ADC1->LTRH = 0x00;								//analog watchdog threshold low
	ADC1->LTRL = 0x00;								//analog watchdog threshold low
	ADC1->AWCRH = 0x00;								//analog watchdog disabled
	ADC1->AWCRL = 0x00;								//analog watchdog disabled
	
	ADC1_Enable();
	
	enableInterrupts();
}

void ADC1_Enable(void)
{
	ADC1->CR1 |= ADC1_CR1_ADON;						//first time it enables the ADC
}

void ADC1_Disable(void)
{
	ADC1->CR1 &= (uint8_t) ~ADC1_CR1_ADON;
}

void ADC1_SoftTrigConv(void)
{
	ADC1->CSR &= (uint8_t) ~ADC1_CSR_EOC;			//this also erases the channel number 
	ADC1->CSR |= ADC1_nr_of_channels - 1;			//so it must be rewritten (RM0016 pag.428)
	ADC1->CR1 |= ADC1_CR1_ADON;						//second time it triggers a conversion
}


uint16_t ADC1_ReadBuffer(uint8_t Buffer)
{
	uint16_t temph = 0;
	uint8_t templ = 0;

    templ = *(uint8_t*)(uint16_t)((uint16_t)ADC1_BaseAddress + (uint8_t)(Buffer << 1) + 1);
    temph = *(uint8_t*)(uint16_t)((uint16_t)ADC1_BaseAddress + (uint8_t)(Buffer << 1));
    temph = (uint16_t)(templ | (uint16_t)(temph << (uint8_t)8));
	return ((uint16_t)temph);
}
#ifndef STM8_CLK_HSE_H
#define STM8_CLK_HSE_H

#include "stm8s.h"

void CLK_HSE_DeConfig(void);
void CLK_HSE_Config(void);

#endif

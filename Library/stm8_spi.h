#ifndef STM8_SPI_H
#define STM8_SPI_H

#include "stm8s.h"
#include "stm8s_spi.h"

void SPI_Config(void);
void SPI_Send_16bits(uint16_t data);

#endif

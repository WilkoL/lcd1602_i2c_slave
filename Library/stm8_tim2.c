#include "stm8_tim2.h"

void TIM2_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 |= CLK_PCKENR1_TIM2;		//enable TIMER2 clock
	
	TIM2_DeInit();							//see stm8s_tim1.h (in SPL)
	
	TIM2->CR1 |= TIM2_CR1_ARPE;				//ARR register buffered through a preload register
	
	//TIM2->IER |= TIM2_IER_UIE;				//interrupt on update
	
	TIM2->CCMR3 |= (6<<4);					//OC3M = 110 : PWM mode 1
	TIM2->CCMR3 |= TIM2_CCMR_OCxPE;			//Preload register on TIMx_CCR3 enabled
	TIM2->CCMR3 |= 0;						//CC3S = 00 : CC3 channel is configured as output
	
	TIM2->CCER2 |= TIM2_CCER2_CC3E;			//CC3 output enable
	
	TIM2->CNTRH = 0;						//set counter to 0
	TIM2->CNTRL = 0;
	
	TIM2->PSCR = 7;							//prescaler = 2^7 (16MHz / 128 = 125000Hz
	
	TIM2->ARRH = 0x00;						//ARR = 125			(1 kHz)
	TIM2->ARRL = 0x7C;
	
	TIM2->CCR3H = 0x00;						//compare1 = 63
	TIM2->CCR3L = 0x3F;
	
	TIM2->CR1 |= TIM2_CR1_CEN;				//counter enabled
		
	enableInterrupts();
}

#ifndef STM8_BEEP_H
#define STM8_BEEP_H

#include "stm8s.h"

void BEEP_Config(void);
void BEEP_On_On(void);
void BEEP_Off(void);

#endif

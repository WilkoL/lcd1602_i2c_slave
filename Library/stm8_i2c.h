#ifndef STM8_I2C_H_H
#define STM8_I2C_H_H

#include "stm8s.h"
#include "stm8s_i2c.h"

void I2C_Config_Master(void);

void I2C_sendbyte_reg8bits(uint8_t reg8bits_address, uint8_t reg8bits_register, uint8_t databyte);
uint8_t I2C_readbyte_reg8bits(uint8_t reg8bits_address, uint8_t reg8bits_register);

void I2C_sendarray_reg8bits(uint8_t reg8bits_address, uint8_t reg8bits_register,  uint8_t *dataarray, uint8_t datalength);
void I2C_readarray_reg8bits(uint8_t reg8bits_address, uint16_t reg8bits_register, uint8_t *dataarray, uint8_t datalength);

void I2C_sendbyte_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register, uint8_t reg16bits_databyte);
uint8_t I2C_readbyte_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register);

void I2C_sendarray_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register,  uint8_t *dataarray, uint8_t datalength);
void I2C_readarray_reg16bits(uint8_t reg16bits_address, uint16_t reg16bits_register, uint8_t *dataarray, uint8_t datalength);

#endif

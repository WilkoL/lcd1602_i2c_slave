#ifndef STM8_MAX7219_H
#define STM8_MAX7219_H

#include "stm8s.h"
#include "stm8_spi.h"

void MAX7219_Config(void);
void MAX7219_shownumber(uint32_t number, uint8_t decimal_point);
void MAX7219_brightness(uint8_t brightness);

#endif

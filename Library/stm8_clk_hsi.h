#ifndef STM8_CLK_HSI_H
#define STM8_CLK_HSI_H

#include "stm8s.h"

void CLK_HSI_DeConfig(void);
void CLK_HSI_Config(void);

#endif

#include "stm8_tim1.h"

void TIM1_DeConfig(void)
{
  TIM1->CR1  = TIM1_CR1_RESET_VALUE;
  TIM1->CR2  = TIM1_CR2_RESET_VALUE;
  TIM1->SMCR = TIM1_SMCR_RESET_VALUE;
  TIM1->ETR  = TIM1_ETR_RESET_VALUE;
  TIM1->IER  = TIM1_IER_RESET_VALUE;
  TIM1->SR2  = TIM1_SR2_RESET_VALUE;
  /* Disable channels */
  TIM1->CCER1 = TIM1_CCER1_RESET_VALUE;
  TIM1->CCER2 = TIM1_CCER2_RESET_VALUE;
  /* Configure channels as inputs: it is necessary if lock level is equal to 2 or 3 */
  TIM1->CCMR1 = 0x01;
  TIM1->CCMR2 = 0x01;
  TIM1->CCMR3 = 0x01;
  TIM1->CCMR4 = 0x01;
  /* Then reset channel registers: it also works if lock level is equal to 2 or 3 */
  TIM1->CCER1 = TIM1_CCER1_RESET_VALUE;
  TIM1->CCER2 = TIM1_CCER2_RESET_VALUE;
  TIM1->CCMR1 = TIM1_CCMR1_RESET_VALUE;
  TIM1->CCMR2 = TIM1_CCMR2_RESET_VALUE;
  TIM1->CCMR3 = TIM1_CCMR3_RESET_VALUE;
  TIM1->CCMR4 = TIM1_CCMR4_RESET_VALUE;
  TIM1->CNTRH = TIM1_CNTRH_RESET_VALUE;
  TIM1->CNTRL = TIM1_CNTRL_RESET_VALUE;
  TIM1->PSCRH = TIM1_PSCRH_RESET_VALUE;
  TIM1->PSCRL = TIM1_PSCRL_RESET_VALUE;
  TIM1->ARRH  = TIM1_ARRH_RESET_VALUE;
  TIM1->ARRL  = TIM1_ARRL_RESET_VALUE;
  TIM1->CCR1H = TIM1_CCR1H_RESET_VALUE;
  TIM1->CCR1L = TIM1_CCR1L_RESET_VALUE;
  TIM1->CCR2H = TIM1_CCR2H_RESET_VALUE;
  TIM1->CCR2L = TIM1_CCR2L_RESET_VALUE;
  TIM1->CCR3H = TIM1_CCR3H_RESET_VALUE;
  TIM1->CCR3L = TIM1_CCR3L_RESET_VALUE;
  TIM1->CCR4H = TIM1_CCR4H_RESET_VALUE;
  TIM1->CCR4L = TIM1_CCR4L_RESET_VALUE;
  TIM1->OISR  = TIM1_OISR_RESET_VALUE;
  TIM1->EGR   = 0x01; /* TIM1_EGR_UG */
  TIM1->DTR   = TIM1_DTR_RESET_VALUE;
  TIM1->BKR   = TIM1_BKR_RESET_VALUE;
  TIM1->RCR   = TIM1_RCR_RESET_VALUE;
  TIM1->SR1   = TIM1_SR1_RESET_VALUE;
}


void TIM1_Config(void)
{
	disableInterrupts();
	
	CLK->PCKENR1 |= CLK_PCKENR1_TIM1;				//enable TIMER1 clock
		
	TIM1_DeInit();
	
	TIM1->CR1 = (TIM1_CR1_ARPE | TIM1_CR1_CEN);		//TIM1_ARR buffered, TIM1 enabled
	TIM1->CR2 = (0x02 << 4);						//TRGO on update, geen capture/compare preload
		
	TIM1->SMCR = 0x00;								//geen TRGI, geen clock/trigger/slave mode
	TIM1->ETR = 0x00;								//geen externe clock
		
	TIM1->IER = TIM1_IER_UIE;						//Update interrupt enabled
	TIM1->EGR = 0x00;								//geen self-triggering events generated
		
	TIM1->CCMR1 = 0x00;								//geen output compare mode
	TIM1->CCMR2 = 0x00;
	TIM1->CCMR3 = 0x00;
	TIM1->CCMR4 = 0x00;
		
	TIM1->CCER1 = 0x00;								//geen in/output capture/compare mode
	TIM1->CCER2 = 0x00;
		
	TIM1->PSCRH = 0x06;			//1599				//prescaler high byte				//10 kHz
	TIM1->PSCRL = 0x3F;								//prescaler low byte
		
	TIM1->ARRH = 0x27;			//9999				//auto reload register high byte	//1 Hz
	TIM1->ARRL = 0x0F;								//auto reload register low byte
		
	TIM1->RCR = 0x00;								//repetition counter value

	TIM1->BKR = 0x00;								//master output off, geen break
	TIM1->DTR = 0x00;								//deadtime register
	TIM1->OISR = 0x00;								//output idle state
	
	enableInterrupts();
}
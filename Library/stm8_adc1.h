#ifndef STM8_ADC1_H
#define STM8_ADC1_H

#include "stm8s.h"

extern volatile uint16_t ADC1_result[];
extern volatile uint8_t ADC1_conv_ready;
extern const uint8_t ADC1_nr_of_channels;

void ADC1_DeConfig(void);
void ADC1_Config(void);
void ADC1_Enable(void);
void ADC1_Disable(void);
void ADC1_SoftTrigConv(void);
uint16_t ADC1_ReadBuffer(uint8_t Buffer);


#endif

#ifndef STM8_TIM1_H
#define STM8_TIM1_H

#include "stm8s.h"

void TIM1_DeConfig(void);
void TIM1_Config(void);

#endif

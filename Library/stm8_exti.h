#ifndef STM8_EXTI_H
#define STM8_EXTI_H

#include "stm8s.h"

extern volatile uint8_t exti_on_portc;

void EXTI_Config(void);

#endif

#ifndef STM8_I2C_SLAVE_H
#define STM8_I2C_SLAVE_H

#define SLAVE_ADDRESS 0x60

#include "stm8s.h"
#include "stm8s_i2c.h"



void I2C_Config_Slave(void);


#endif

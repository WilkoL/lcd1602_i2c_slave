#ifndef __MAIN_H
#define __MAIN_H

enum DATASOORT {wis, locatie, letters, decimaal, hexadecimaal};
enum DATASOORT datasoort;

#include <stm8s.h>
#include <stdio.h>

#include "stm8_clk_hsi.h"
#include "stm8_systick_delay.h"
#include "stm8_gpio.h"
#include "stm8_hd44780.h"
#include "stm8_i2c_slave.h"
#include "stm8_tim1.h"
#include "stm8_tim2.h"

#endif
/*
* STM8S103F3 @ 16 MHz interne oscillator
* display I2C data op een 16x4 LCD
*
* SLAVE_ADDRESS 0x60 (8 bits notatie)
* SLAVE_ADDRESS 0x30 (7 bits notatie)
*
* data formaat: i2s-address, x-coord, y_coord, data1, data2, data....
* uiteraard maximaal 16 bytes data lang en maximaal 4 regels (0, 1, 2, 3)
*
* Arduino voorbeeld:
* 
* 	Wire.beginTransmission(0x30);	//start met slave-address
*  	Wire.write(3);					//x-coordinaat
*  	Wire.write(0);					//y-coordinaat
*  	Wire.write("x is ");			//5 bytes data
*  	itoa(x, buffer, 10);			//getal als ascii data
*  	Wire.write(buffer); 			//
*	Wire.endTransmission();			//stop
*
* wanneer data binnen is wordt dit direct naar de LCD1602 gestuurd
* wanneer er 5 seconden lang geen data binnenkomt, gaat de stm8
* elke seconde de LCD1602 "refreshen".
*
* verwerken van 16x4 bytes op de LCD1604 duurt ongeveer 10 ms
*
* op pin10 (PA3, TIM2_CH3) staat een blokgolf van 1000 Hz, 50% duty cycle
*/


#include "main.h"


extern volatile uint8_t LCD_buffer[16][4];
extern volatile uint8_t token;

//extern volatile uint8_t num8;
//extern volatile uint16_t num16;
//extern volatile uint32_t num32;

//extern volatile uint8_t x;
//extern volatile uint8_t y;

int main(void)
{
	char buffer[20];
	//int8_t i = 0;
	
	CLK_HSI_Config();
	SYSTICK_Config();
	GPIO_Config();
	
	HD44780_init(HD44780_DISPLAY_ON_CURSOR_OFF_BLINK_OFF);
	I2C_Config_Slave();
	TIM1_Config();
	TIM2_Config();
	
	HD44780_clrscr();
	HD44780_gotoxy(2,0);
	HD44780_puts("regel 0");
	HD44780_gotoxy(4,1);
	HD44780_puts("regel 1");
	HD44780_gotoxy(6,2);
	HD44780_puts("regel 2");
	HD44780_gotoxy(8,3);
	HD44780_puts("regel 3");
	DELAY_ms(4000);
	

	token = 0x01;

	while (1)
	{
		if (token & 0x01)										//clear screenbuffer & screen
		{
			uint8_t i;
			uint8_t j;

			for (j = 0; j < 4; j++)
			{
				for (i = 0; i < 20; i++)
				{
					LCD_buffer[i][j] = 0x20;
				}
			}
			HD44780_clrscr();
			token &= (uint8_t) ~(0x01);
		}



		else if (token & 0x02)									//display screenbuffer
		{
			uint8_t i;
			uint8_t j;

			for (j = 0; j < 4; j++)
			{
				HD44780_gotoxy(0,j);
				for (i = 0; i < 20; i++)
				{
					HD44780_putc(LCD_buffer[i][j]);
				}
			}
			token &= (uint8_t) ~(0x02);
		}
	}
}




#ifdef USE_FULL_ASSERT

	void assert_failed(uint8_t* file, uint32_t line)
	{
		while (1)
		{
		}
	}
#endif
#include "stm8s_it.h"
#include "stm8_hd44780.h"

enum datasoort {wis, locatie, letters, decimaal, hexadecimaal};

union voegsamen
{
	uint32_t num;
	uint8_t bytes[4];
};

volatile uint8_t LCD_buffer[16][4];
volatile uint8_t token = 0;
volatile uint8_t timeout = 0;


#ifdef _COSMIC_

INTERRUPT_HANDLER(NonHandledInterrupt, 25)
{

}
#endif


INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
{

}


INTERRUPT_HANDLER(TLI_IRQHandler, 0)

{

}


INTERRUPT_HANDLER(AWU_IRQHandler, 1)
{
	/*
	if (AWU->CSR & AWU_CSR_AWUF);				//wakeup occured (flag is reset by reading it)
	*/
}


INTERRUPT_HANDLER(CLK_IRQHandler, 2)
{

}


INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
{

}


INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
{

}


INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
{

}


INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
{

}


INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{

}

#if defined (STM8S903) || defined (STM8AF622x) 

 INTERRUPT_HANDLER(EXTI_PORTF_IRQHandler, 8)
 {

 }
#endif

#if defined (STM8S208) || defined (STM8AF52Ax)

 INTERRUPT_HANDLER(CAN_RX_IRQHandler, 8)
 {

 }


 INTERRUPT_HANDLER(CAN_TX_IRQHandler, 9)
 {

 }
#endif


INTERRUPT_HANDLER(SPI_IRQHandler, 10)
{

}


INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
{
	if (TIM1->SR1 & TIM1_SR1_UIF)
	{
		TIM1->SR1 &= (uint8_t) ~TIM1_SR1_UIF;		//clear interrupt on update flag
		if (timeout > 4) token |= 0x02;
		else timeout++;
	}
}


INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
{

}

#if defined (STM8S903) || defined (STM8AF622x)

 INTERRUPT_HANDLER(TIM5_UPD_OVF_BRK_TRG_IRQHandler, 13)
 {

 }
 

 INTERRUPT_HANDLER(TIM5_CAP_COM_IRQHandler, 14)
 {

 }

#else

 INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
 {
	if (TIM2->SR1 & TIM2_SR1_UIF)
	{
		TIM2->SR1 &= (uint8_t) ~TIM2_SR1_UIF;		//clear interrupt on update flag
	}
 }


 INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
 {

 }
#endif

#if defined (STM8S208) || defined(STM8S207) || defined(STM8S007) || defined(STM8S105) || \
    defined(STM8S005) ||  defined (STM8AF62Ax) || defined (STM8AF52Ax) || defined (STM8AF626x)

 INTERRUPT_HANDLER(TIM3_UPD_OVF_BRK_IRQHandler, 15)
 {

 }


 INTERRUPT_HANDLER(TIM3_CAP_COM_IRQHandler, 16)
 {

 }
#endif







#if defined (STM8S208) || defined(STM8S207) || defined(STM8S007) || defined(STM8S103) || \
    defined (STM8S003) || defined(STM8S001) || defined (STM8AF62Ax) || defined (STM8AF52Ax) || defined (STM8S903)

INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
{
	/*
	uint8_t tmp;
	
	if (UART1->SR & UART1_SR_TXE)
	{
		if (UART1_txhead == UART1_txtail)
		{
			UART1->CR2 &= (uint8_t) ~UART1_CR2_TIEN;//disable TXE-irq: tx buffer empty
		}
		else
		{
			tmp = (uint8_t) ((UART1_txtail + 1) & UART1_txbuff_mask);
			UART1_txtail = tmp;
			UART1->DR = UART1_txbuff[tmp];			//send next byte from tx buffer
		}
	}
	*/
}



 INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
 {
	 /*
	uint8_t tmp;
	uint8_t data;
	 
	if (UART1->SR & UART1_SR_RXNE)
	{
		UART1_rxerror = 0;
		data = (uint8_t) UART1->DR;
		tmp = (uint8_t) ((UART1_rxhead + 1) & UART1_rxbuff_mask);
		
		if (tmp == UART1_rxtail)					//buffer full!
		{
			UART1_rxerror = 1;
		}
		else
		{
			UART1_rxbuff[tmp] = data;
			UART1_rxhead = tmp;
		}
	}
	*/
 }
#endif











#if defined(STM8AF622x)

 INTERRUPT_HANDLER(UART4_TX_IRQHandler, 17)
 {

 }


 INTERRUPT_HANDLER(UART4_RX_IRQHandler, 18)
 {

 }
#endif


INTERRUPT_HANDLER(I2C_IRQHandler, 19)
{
	union voegsamen uitvoer;
	int8_t i = 0;
	static uint8_t x = 0;
	static uint8_t y = 0;
	static uint8_t data_len = 0;
	static uint8_t i2c_buffer[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	static uint16_t Event = 0x00;
	uint8_t cijfers[16];
	uint8_t startdigit = 0;
	uint32_t getal;

	if ((I2C->SR2) != 0)								//Read SR2 register to get I2C error
	{
		I2C->SR2 = 0;									//Clears SR2 register
	}
  
	Event = (uint16_t) I2C_GetLastEvent();
	switch (Event)
	{
														/******* Slave transmitter ******/
	case I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED:	//check on EV1
		data_len = 0;
		timeout = 0;
		break;
	  
    case I2C_EVENT_SLAVE_BYTE_TRANSMITTING:				//check on EV3
		I2C_SendData(i2c_buffer[data_len]);				//Transmit data
		break;
	  
														/******* Slave receiver **********/
    case I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED:		//check on EV1
		data_len = 0;
		break;

    case I2C_EVENT_SLAVE_BYTE_RECEIVED:					//Check on EV2
		i2c_buffer[data_len++] = I2C_ReceiveData();
		break;
	  
	  
	  

    case (I2C_EVENT_SLAVE_STOP_DETECTED):				//Check on EV4
		I2C->CR2 |= I2C_CR2_ACK;						//write to CR2 to clear STOPF flag
		
		
		if (i2c_buffer[0] == locatie)
		{
			x = i2c_buffer[1];									//geen signaal naar main
			y = i2c_buffer[2];
		}


		else if (i2c_buffer[0] == wis)
		{
			token |= 0x01;										//wis_signaal naar main
		}


		else if (i2c_buffer[0] == letters)
		{
			for (i = 0; i < (data_len - 1); i++)
			{
				LCD_buffer[x++][y] = i2c_buffer[i+1];
			}
			token |= 0x02;										//show_char_signaal naar main
		}


		else if (i2c_buffer[0] == decimaal)						//calculate dec
		{
			uint8_t temp;

			uitvoer.bytes[0] = i2c_buffer[1];
			uitvoer.bytes[1] = i2c_buffer[2];
			uitvoer.bytes[2] = i2c_buffer[3];
			uitvoer.bytes[3] = i2c_buffer[4];
			getal = uitvoer.num;

			for (i = 9; i >= 0; i--)
			{
				temp = (uint8_t) (getal % 10);
				cijfers[i] = temp;
				getal /= 10;
				if (temp > 0) startdigit = i;
			}

			for (i = startdigit; i < 10; i++)
			{
				LCD_buffer[x++][y] = (uint8_t) (cijfers[i] + 48);
			}

			token |= 0x02;
		}


		else if (i2c_buffer[0] == hexadecimaal)					//calculate hex
		{
			uint8_t temp;

			LCD_buffer[x++][y] = 48;							//0
			LCD_buffer[x++][y] = 120;							//x

			for (i = 1; i < (data_len); i++)
			{
				temp = (uint8_t) ((i2c_buffer[i] & 0xF0) >> 4);
				if (temp < 0x0A) LCD_buffer[x++][y] = (uint8_t) (temp + 48);
				else LCD_buffer[x++][y] = (uint8_t) (temp + 55);

				temp = (uint8_t) (i2c_buffer[i] & 0x0F);
				if (temp < 0x0A) LCD_buffer[x++][y] = (uint8_t) (temp + 48);
				else LCD_buffer[x++][y] = (uint8_t) (temp + 55);
			}
			token |= 0x02;
		}
		break;

    default:
		break;
	}
}

#if defined(STM8S105) || defined(STM8S005) ||  defined (STM8AF626x)

 INTERRUPT_HANDLER(UART2_TX_IRQHandler, 20)
 {

 }


 INTERRUPT_HANDLER(UART2_RX_IRQHandler, 21)
 {

 }
#endif

#if defined(STM8S207) || defined(STM8S007) || defined(STM8S208) || defined (STM8AF52Ax) || defined (STM8AF62Ax)

 INTERRUPT_HANDLER(UART3_TX_IRQHandler, 20)
 {

 }


 INTERRUPT_HANDLER(UART3_RX_IRQHandler, 21)
 {

 }
#endif

#if defined(STM8S207) || defined(STM8S007) || defined(STM8S208) || defined (STM8AF52Ax) || defined (STM8AF62Ax)

 INTERRUPT_HANDLER(ADC2_IRQHandler, 22)
 {

 }
#else

 INTERRUPT_HANDLER(ADC1_IRQHandler, 22)
 {
	 /*
	uint16_t temph = 0;
	uint8_t templ = 0;
	uint8_t i = 0;
	
	if (ADC1->CSR & ADC1_CSR_EOC)
	{
		ADC1->CSR &= (uint8_t) ~ADC1_CSR_EOC;			//this also erases the channel number 
		ADC1->CSR |= (uint8_t) (ADC1_nr_of_channels - 1);//so must be rewritten (RM0016 pag.428) 

		for (i = 2; i < (ADC1_nr_of_channels); i++)		//ch0 and ch1 do not exist on stm8f003f3
		{
			templ = *(uint8_t*)(uint16_t)((uint16_t)ADC1_BaseAddress + (uint8_t)(i << 1) + 1);
			temph = *(uint8_t*)(uint16_t)((uint16_t)ADC1_BaseAddress + (uint8_t)(i << 1));
			ADC1_result[i] = (uint16_t)(templ | (uint16_t)(temph << (uint8_t)8));
		}
		ADC1_conv_ready = 1;
	}
	*/
 }
#endif

#if defined (STM8S903) || defined (STM8AF622x)

INTERRUPT_HANDLER(TIM6_UPD_OVF_TRG_IRQHandler, 23)
 {

 }

#else

 INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
 {
	TIM4->SR1 &= (uint8_t)~(TIM4_SR1_UIF);					//clear interrupt on update flag
	millis++;
	countflag = 1;											//every ms set to 1
 }

#endif


INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
{

}

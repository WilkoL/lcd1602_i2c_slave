


COPY ALL FILES TO PROJECT FOLDER

the STDLib is used for 
stm8s.h
stm8s_conf.h 
stm8s_gpio.h 						(for friendly_names such as "GPIO_PIN_0")
stm8s_i2c.h, stm8s_i2c.c
stm8s_spi.c, stm8s_spi.h


FILES to ADD to project

SOURCE FILES
In source files already are:
- main.c 							(should already be in Source Files)
- stm8_interrupt_vector				(should already be in Source Files)

add these files to Source Files:
- stm8s_it.c						(in project)
- stm8_gpio.c 						(in library) (i2c ports are always open-drain!)
- stm8_systick_delay.c				(in library)
- stm8_clk_hse.c					(in library) (or the hsi version)

IF NEEDED add these files to Source Files:
- stm8_uart1.c						(in library)
- stm8_adc1.c 						(in library)
- stm8_i2c.c 						(in library)
- stm8_spi.c 						(in library)
- stm8_max7219.c 					(in library)
- stm8_tim1.c 						(in library)
- stm8s_i2c.c						(in STDPeriph / src
- stm8s_spi.c						(in STDPeriph / src





INCLUDE FILES
add these files to Include Files:
- main.h							(in project)
- stm8s_it.h						(in project)
- stm8_gpio.h 						(in library)
- stm8_systick_delay.h				(in library)
- stm8_clk_hse.h					(in library) (or the hsi version)
- stm8s.h							(in STDPeriph / inc)
- stm8s_conf.h						(in STDPeriph / inc)

IF NEEDED add these files to Include Files:
- stm8_uart1.h 						(in library)
- stm8_tim1.h 						(in library)
- stm8_adc1.h 						(in library)
- stm8_i2c.h 						(in library)
- stm8_spi.h						(in library)
- stm8_max7219.h 					(in library)
- stm8_tim1.h  						(in library)
- stm8s_i2c.h						(in STDPeriph / inc)
- stm8s_spi.h 						(in STDPeriph / inc)




stm8s_conf.h is already editted to exclude USE_FULL_ASSERT)
stm8s_it.c is already editted to include TIMER4 UPDATE IRQ handling
	(for systick and DELAY) 

IF NEEDED edit stm8s_it.c to include
- UART1 Rx and TX handling
- TIM1 handling
- ADC1 handling
- AWU handling

go to:
Project -> Settings -> General
- choose "Project specific toolset path"
- change Root path into: C:\Program Files (x86)\COSMIC\FSE_Compilers\CXSTM8
do this for both Debug and Release Settings
click OK

go to: 
Project -> Settings -> C-Compiler -> Category General
- add STM8S003 HSE_VALUE=16000000UL to the Preprocessor Definitions (the MCU and its CLOCK_freq)
- change Compiler Message Display from "Display errors only" to "Display errors & Warnings"
- add +split to User Defined Options
do this for both Debug and Release Settings
click OK

go to: 
Project -> Settings -> Linker 
- in the Category  Output "Generate Map file"
do this for both Debug and Release Settings
click OK

try REBUILD_ALL to check for warnings / errors

go to:
Tools -> Programmer -> Settings: change Hardware to "ST-LINK"
Tools -> Programmer -> Memory Areas -> DATA MEMORY: should be empty
Tools -> Programmer -> Memory Areas -> PROGRAM MEMORY -> Add: projectname\Debug\xxx.s19 file
(or the .s19 file in projectname\Release after building for Release

The option bytes are interesting to check but ususally are correct.

Click OK (or you will keep setting the programmer options)

Tools -> Programmer -> Program  click "Start" to upload the code to the MCU